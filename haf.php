<?php
header('Content-Type: text/html');
require_once("parsedown.php");
$parsedown = new Parsedown();
$die_end = "\n\t</body>\n</html>\n<!--Stop it..-->\n"; // Almost feels like it's necessary
$current_dir = __DIR__.$_SERVER['REQUEST_URI'];
$is_header = !isset($_GET["f"]); // Look how proficiently inverted boolean.

/* Remove possible $_GET data from dir variable */
if (strstr($current_dir, '?'))
	$current_dir = substr($current_dir, 0, strpos($current_dir, "?"));

/* Custon dir name, despite .md extension it's not parsed */
if (file_exists($current_dir.".name.md"))
	$folder_name = file_get_contents($current_dir.".name.md");
else
	$folder_name = htmlspecialchars(end(array_filter(explode("/", $current_dir))));

if ($is_header) {
	echo "<!DOCTYPE html>\n";
	echo "<html>\n";
	echo "\t<head>\n";
	echo "\t\t<meta charset=\"UTF-8\">\n";
	echo "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/autoindex.css?time=".time()."\" />\n";

	if(file_exists($current_dir.".style.css"))
		echo "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\".style.css?a=12\" />\n";

	echo "\t</head>\n";
	echo "\t<body>\n";

	/* Let's stop meanies */
	if (strpos($_SERVER['REQUEST_URI'], "..")) // L33T parent dir hack prevention
		die("Stop hacking my server.".$die_end);

	if ($_SERVER['PHP_SELF'] == $_SERVER['REQUEST_URI']) // File should be viewed only when included
		die("Nothing to see here.".$die_end);

	if (mb_substr($current_dir, -1) != "/") // You can never be too sure
		die("The fuck? How?".$die_end);
	
	echo "\t\t<h1>$folder_name</h1>\n";
}else {
	echo "<br />\n";
}

/* Very skilled ternary puke */
echo $parsedown->text(file_get_contents($current_dir.($is_header?".header":".footer").".md"))."\n\n" ?? NULL;

if ($is_header) {
	echo "\t\t<hr />\n\n";
}else {
	echo "\t</body>";
	echo "</html>\n";
}
