# Autoindex
This is my quickly made system to make nginx (fancy_)autoindex to be better in my view.

## What can it do?
Most obivious for end user: dark theme and icons for different filetypes.

Then folder specific stuff, these files need to be in folder they should alter. They are hidden in listing.
  - .style.css file will, you guessed it, be included to index page
  - .header.md will be included on top of page and conents of it will me formatted like markdown should be
  - .footer.md will do just same, just in end of index
Header and footer will be created no matter of those filest existing. They will make clean html page with <html> etc. tags.
.nginx.conf has only part what matters regarding this project, personally I have /files/ folder that will be used, and I leave that in repo. And please do not ask me about that rewrite in there, it works, don't touch it.
Every example are just plain copypaste from my folder because I am lazy.

Icons will come later to this repo.

I will later add password protection possibility to dirs.